<!DOCTYPE html>
<html>
<head>
    <title>Time Management</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
</head>
<body>
    <div class="row mt-5">
        @if (session()->has('message'))
        <div class="col">
            <div class="alert alert-success text-center">
                {{ session()->get('message') }}
            </div>
        </div>
        @endif

        <div class="col-md-10 offset-md-1">
            <h3 class="text-center mb-4"> My Tasks  <b>
            <a href="{{ route('home') }}" class="btn btn-success">Create Task</a>
            </b></h3>

            <table id="table" class="table table-bordered">
              <thead>
                <tr>
                  <th width="30px">#</th>
                  <th>Task</th>
                  <th>Deadline</th>
                  <th>Created At</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="tablecontents">
                @foreach($posts as $post)
    	            <tr class="row1" data-id="{{ $post->id }}">
    	              <td class="pl-3"><i class="fa fa-sort"></i></td>
                      <td>{{ $post->task }}</td>
                      <td>{{ $post->deadline }}</td>
                      <td>{{ date('d-m-Y h:m:s',strtotime($post->created_at)) }}</td>
                      <td>
                        <a  href="{{ route('post.edit', $post->id) }}" class="btn btn-primary">Edit Task</a>
                        <a onclick="return confirm('Are you sure want to delete this post?')" href="{{ route('post.destroy', $post->id) }}" class="btn btn-danger">Delete Task</a>
                      </td>
    	            </tr>
                @endforeach
              </tbody>
            </table>
            <hr>
            <h5>Drag and Drop the table rows to make changes if you wish and click <button class="btn btn-success btn-sm" onclick="window.location.reload()">REFRESH</button> the page to re-arrange.</h5>
    	</div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>
    <script type="text/javascript">
      $(function () {
        $("#table").DataTable();

        $( "#tablecontents" ).sortable({
          items: "tr",
          cursor: 'move',
          opacity: 0.6,
          update: function() {
              sendOrderToServer();
          }
        });

        function sendOrderToServer() {
          var order = [];
          var token = $('meta[name="csrf-token"]').attr('content');
          $('tr.row1').each(function(index,element) {
            order.push({
              id: $(this).attr('data-id'),
              position: index+1
            });
          });

          $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{ url('post-sortable') }}",
                data: {
              order: order,
              _token: token
            },
            success: function(response) {
                if (response.status == "success") {
                  console.log(response);
                } else {
                  console.log(response);
                }
            }
          });
        }
      });
    </script>
</body>
</html>
{{-- Reference --}}
{{-- https://www.nicesnippets.com/blog/laravel-6-drag-and-drop-datatable-rows-for-sorting-example --}}
