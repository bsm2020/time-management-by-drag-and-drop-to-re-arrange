Getting started
Installation
Please check the official laravel installation guide for server requirements before you start. Official Documentation

Clone the repository

git clone https://bsm2020@bitbucket.org/bsm2020/time-management-by-drag-and-drop-to-re-arrange.git
Switch to the repo folder

cd laravel-realworld-example-app
Install all the dependencies using composer

composer install
Copy the example env file and make the required configuration changes in the .env file

cp .env.example .env
Generate a new application key

php artisan key:generate
Generate a new authentication secret key


php artisan migrate  -seed
Start the local development server

php artisan serve
You can now access the server at http://localhost:8000

TL;DR command list

git clone git@github.com:gothinkster/laravel-realworld-example-app.git
cd laravel-realworld-example-app
composer install
cp .env.example .env
php artisan key:generate
Make sure you set the correct database connection information before running the migrations Environment variables

php artisan migrate
php artisan serve

After Serving from local machine,
Click on register to sign up for first time use

click on log in for subsequence use.

Home page allows user to view all tasks and to create new ones

All post pages allows users to drag and drop to re-arrange task, edit or delete a task

The create task form has three fields namely, title, deadline and sort.

{{-- Reference --}}
{{-- https://www.nicesnippets.com/blog/laravel-6-drag-and-drop-datatable-rows-for-sorting-example --}}
