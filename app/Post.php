<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [
        // 'task', 'order','deadline', 'user_id',
    ];
     protected $casts=['deadline'=>'string'];

      //Relationship between user table and post table
    public function user()
    {
    return $this->belongsTo('App\User');
    }
}
