<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'task' => $faker->sentence(),
        'deadline' => $faker->date(),
        'user_id'=>$faker->numberBetween(1,5),
        'order' =>$faker->randomDigitNotNull() ,
    ];
});

