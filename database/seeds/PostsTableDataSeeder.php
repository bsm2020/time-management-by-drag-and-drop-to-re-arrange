<?php

use App\Post;
use Illuminate\Database\Seeder;

class PostsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Post::class,50)->create();
    }
}
